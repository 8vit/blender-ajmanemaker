use alt_fp::FloatOrdSet;
use array::{Array2 as _, Array3 as _};
use cgmath::{prelude::*, Point3, Vector3};
use ndarray::Array3;
use std::{
    cmp::{max, min},
    collections::HashSet,
};

use crate::{
    tri::tricrast,
    utils::{point3_max, point3_min, point_to_tri_barycentric, TriangleFan},
};

#[derive(Debug)]
pub struct Domain {
    domain_min: Point3<f32>,
    domain_max: Point3<f32>,
    cell_size: Vector3<f32>,
    field_type: Array3<CellType>,
    field_velocity: Array3<Vector3<f32>>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum CellType {
    Outside,
    Inside,
    Boundary,
}

impl Domain {
    pub fn from_mesh(
        points: Vec<Point3<f32>>,
        points_inflow: Vec<f32>,
        faces: Vec<Vec<usize>>,
        num_iterations: usize,
        domain_res: usize,
    ) -> Result<Self, String> {
        assert_eq!(points.len(), points_inflow.len());

        let domain_min = point3_min(points.iter().cloned());
        let domain_max = point3_max(points.iter().cloned());

        // Inflate the domain a skosh
        let (domain_min, domain_max) = {
            let size = domain_max - domain_min;
            (domain_min - size * 0.05, domain_max + size * 0.05)
        };

        let cell_size = (domain_max - domain_min) * (1.0 / domain_res as f32);

        // Reify the boundary
        #[derive(Debug, Clone, Copy)]
        struct BoundaryPoint {
            point: Point3<u8>,
            normal: Vector3<f32>,
            flow: f32,
        }

        let boundary_points = {
            let mut boundary_points = Vec::new();
            let mut z_buffer = vec![0.0..0.0; domain_res];

            let scale =
                Vector3::from([domain_res as f32; 3]).div_element_wise(domain_max - domain_min);

            for face in faces.iter() {
                for tri_points in TriangleFan::new(face.len()) {
                    let tri_points = tri_points.map(|i| face[i]);
                    let tri_co = tri_points.map(|i| points[i]);
                    let tri_inflow = tri_points.map(|i| -points_inflow[i]);
                    let normal = (tri_co[1] - tri_co[0])
                        .cross(tri_co[2] - tri_co[0])
                        .normalize();

                    // Transform to grid coordinates
                    let tri_co = tri_co.map(|p| {
                        Point3::new(0.0, 0.0, 0.0) + (p - domain_min).mul_element_wise(scale)
                    });

                    // Rasterize the triangle
                    tricrast(
                        tri_co,
                        [domain_res as u32; 2].into(),
                        &mut z_buffer,
                        |start, z_ranges| {
                            for (x, z_range) in (start.x as u8..).zip(z_ranges.iter()) {
                                let z_min = [z_range.start, 0.0].fmax() as i32;
                                let z_max = [z_range.end, domain_res as f32].fmin().ceil() as i32;
                                for z in z_min..z_max {
                                    let co = Point3::new(x, start.y as u8, z as u8);

                                    // Linear interpolate the inflow amount
                                    let bary =
                                        point_to_tri_barycentric(co.cast::<f32>().unwrap(), tri_co);
                                    let flow = Vector3::from(tri_inflow).dot(bary.into());

                                    boundary_points.push(BoundaryPoint {
                                        point: co,
                                        normal,
                                        flow,
                                    });
                                }
                            }
                        },
                    );
                }
            }

            // Sort the points for deduplication and cache optimization
            boundary_points.sort_by_key(|p| (p.point.x, p.point.y, p.point.z));

            boundary_points
        };

        // Categorize cells
        let mut field_type =
            Array3::<CellType>::from_elem((domain_res, domain_res, domain_res), CellType::Inside);

        for p in boundary_points.iter() {
            let co = p.point.cast::<usize>().unwrap();
            field_type[(co.z, co.y, co.x)] = CellType::Boundary;
        }

        mark_outside(&mut field_type);

        if field_type.iter().all(|x| *x != CellType::Inside) {
            return Err("Could not define the domain region. \
                        Probably the guide mesh is empty or not water-tight."
                .to_owned());
        }

        // Additional shell
        let shell: Vec<_> = field_type
            .indexed_iter()
            .filter_map(|((pz, py, px), &ty)| {
                if ty != CellType::Outside {
                    return None;
                }

                // Find a neighboring boundary cell
                for &[dx, dy, dz] in [
                    [-1isize, 0, 0],
                    [1, 0, 0],
                    [0, -1, 0],
                    [0, 1, 0],
                    [0, 0, -1],
                    [0, 0, 1],
                ]
                .iter()
                {
                    let x = px.wrapping_add(dx as usize);
                    let y = py.wrapping_add(dy as usize);
                    let z = pz.wrapping_add(dz as usize);
                    if x >= domain_res
                        || y >= domain_res
                        || z >= domain_res
                        || field_type[(z, y, x)] != CellType::Boundary
                    {
                        continue;
                    }
                    return Some(([pz, py, px], [z, y, x]));
                }

                None
            })
            .collect();

        // FDM loop
        let mut field_velocity =
            Array3::<Vector3<f32>>::zeros((domain_res, domain_res, domain_res));
        let mut field_temp = Array3::<Vector3<f32>>::zeros((domain_res, domain_res, domain_res));
        let mut field_divergence = Array3::<f32>::zeros((domain_res, domain_res, domain_res));

        for _ in 0..num_iterations {
            // Shell (prevents the outside region from affecting the internal domain)
            for &(p, bp) in shell.iter() {
                field_velocity[p] = field_velocity[bp];
            }

            // Diffusion
            diffuse(&mut field_velocity, &mut field_temp);

            // Shell (prevents the outside region from affecting the internal domain)
            for &(p, bp) in shell.iter() {
                field_velocity[p] = field_velocity[bp];
            }

            // Continuum
            conservation(&mut field_velocity, &mut field_divergence, cell_size);

            // The boundary condition
            for p in boundary_points.iter() {
                let co = p.point.cast::<usize>().unwrap();
                let vel = &mut field_velocity[(co.z, co.y, co.x)];
                *vel = *vel + p.normal * (p.flow - vel.dot(p.normal));
            }
        }

        if false {
            // Debug
            println!("quivers = np.array([");
            for ((z, y, x), vel) in field_velocity.indexed_iter() {
                // println!("  [{}, {}, {}, {}, {}, {}],", x, y, z, vel.x, vel.y, vel.z);
                if z == 8 {
                    println!("  [{}, {}, {}, {}],", x, y, vel.x, vel.y);
                }
            }
            println!("])");
            // println!("x, y, z, u, v, w = zip(*quivers)");
            println!("x, y, u, v = zip(*quivers)");
        }

        Ok(Self {
            domain_min,
            domain_max,
            cell_size,
            field_type,
            field_velocity,
        })
    }

    fn find_inner_cell_in_box(
        &self,
        box_min: Point3<isize>,
        box_max: Point3<isize>,
    ) -> Option<Point3<usize>> {
        let dim = self.field_type.dim();

        for z in max(box_min[2], 0)..min(box_max[2], dim.0 as isize) {
            for y in max(box_min[1], 0)..min(box_max[1], dim.1 as isize) {
                for x in max(box_min[0], 0)..min(box_max[0], dim.2 as isize) {
                    let (x, y, z) = (x as usize, y as usize, z as usize);
                    if self.field_type[(z, y, x)] == CellType::Inside {
                        return Some(Point3::new(x, y, z));
                    }
                }
            }
        }
        None
    }

    /// Find the cell coordinates with cell type `CellType::Inside` closest
    /// to the global coordinates `p`.
    fn find_nearest_inner_cell(&self, p: Point3<f32>) -> Option<Point3<usize>> {
        let cell_size = self.cell_size;

        let pp = Point3::new(0.0, 0.0, 0.0) + (p - self.domain_min).div_element_wise(cell_size);
        let pi = pp.cast::<isize>().unwrap();

        let (mut box_min, mut box_max) = (pi, pi + Vector3::new(1, 1, 1));
        let try_box = |min, max| self.find_inner_cell_in_box(min, max);
        if let Some(x) = try_box(box_min, box_max) {
            return Some(x);
        }

        loop {
            let next_px = ((box_max.x + 1) as f32 - pp.x) * cell_size.x;
            let next_py = ((box_max.y + 1) as f32 - pp.y) * cell_size.y;
            let next_pz = ((box_max.z + 1) as f32 - pp.z) * cell_size.z;
            let next_nx = (pp.x - (box_min.x - 1) as f32) * cell_size.x;
            let next_ny = (pp.y - (box_min.y - 1) as f32) * cell_size.y;
            let next_nz = (pp.z - (box_min.z - 1) as f32) * cell_size.z;
            let earliest = [next_px, next_py, next_pz, next_nx, next_ny, next_nz].fmin();

            // Inflate the box in some direction
            if earliest == next_px {
                if let Some(x) = try_box(
                    Point3::new(box_max.x, box_min.y, box_min.z),
                    Point3::new(box_max.x + 1, box_max.y, box_max.z),
                ) {
                    return Some(x);
                }
                box_max.x += 1;
            } else if earliest == next_nx {
                if let Some(x) = try_box(
                    Point3::new(box_min.x - 1, box_min.y, box_min.z),
                    Point3::new(box_min.x, box_max.y, box_max.z),
                ) {
                    return Some(x);
                }
                box_min.x -= 1;
            } else if earliest == next_py {
                if let Some(x) = try_box(
                    Point3::new(box_min.x, box_max.y, box_min.z),
                    Point3::new(box_max.x, box_max.y + 1, box_max.z),
                ) {
                    return Some(x);
                }
                box_max.y += 1;
            } else if earliest == next_ny {
                if let Some(x) = try_box(
                    Point3::new(box_min.x, box_min.y - 1, box_min.z),
                    Point3::new(box_max.x, box_min.y, box_max.z),
                ) {
                    return Some(x);
                }
                box_min.y -= 1;
            } else if earliest == next_pz {
                if let Some(x) = try_box(
                    Point3::new(box_min.x, box_min.y, box_max.z),
                    Point3::new(box_max.x, box_max.y, box_max.z + 1),
                ) {
                    return Some(x);
                }
                box_max.z += 1;
            } else {
                if let Some(x) = try_box(
                    Point3::new(box_min.x, box_min.y, box_min.z - 1),
                    Point3::new(box_max.x, box_max.y, box_min.z),
                ) {
                    return Some(x);
                }
                box_min.z -= 1;
            }
        }
    }

    fn global_to_cell_f32(&self, p: Point3<f32>) -> Point3<f32> {
        Point3::new(0.0, 0.0, 0.0) + (p - self.domain_min).div_element_wise(self.cell_size)
    }

    fn global_to_cell_isize(&self, p: Point3<f32>) -> Point3<isize> {
        self.global_to_cell_f32(p).cast::<isize>().unwrap()
    }

    /// Get the cell type at the global coordinates `p`.
    fn get_cell_type_global(&self, p: Point3<f32>) -> CellType {
        let pi = self.global_to_cell_isize(p);

        self.field_type
            .get((pi.z as usize, pi.y as usize, pi.x as usize))
            .cloned()
            .unwrap_or(CellType::Outside)
    }

    /// Get the velocity at the global coordinates `p`.
    fn get_velocity_global(&self, p: Point3<f32>) -> Vector3<f32> {
        let p_f = self.global_to_cell_f32(p) - Vector3::new(0.5, 0.5, 0.5);
        let p_i = p_f.cast::<isize>().unwrap();
        let frac = p_f - p_i.cast::<f32>().unwrap();

        let values = [[[0, 1], [2, 3]], [[4, 5], [6, 7]]].map(|a| {
            a.map(|b| {
                b.map(|flags: isize| {
                    let dx = flags >> 2;
                    let dy = (flags & 2) >> 1;
                    let dz = flags & 1;
                    self.field_velocity
                        .get((
                            (p_i.z + dz) as usize,
                            (p_i.y + dy) as usize,
                            (p_i.x + dx) as usize,
                        ))
                        .cloned()
                        .unwrap_or(Vector3::zero())
                })
            })
        });

        // Tri-linear interpolation
        let values = [
            [
                values[0][0][0] * (1.0 - frac.z) + values[0][0][1] * frac.z,
                values[0][1][0] * (1.0 - frac.z) + values[0][1][1] * frac.z,
            ],
            [
                values[1][0][0] * (1.0 - frac.z) + values[1][0][1] * frac.z,
                values[1][1][0] * (1.0 - frac.z) + values[1][1][1] * frac.z,
            ],
        ];

        let values = [
            values[0][0] * (1.0 - frac.y) + values[0][1] * frac.y,
            values[1][0] * (1.0 - frac.y) + values[1][1] * frac.y,
        ];

        values[0] * (1.0 - frac.x) + values[1] * frac.x
    }
}

fn mark_outside(field: &mut Array3<CellType>) {
    let dim = field.dim();
    let mut stack = vec![Point3::new(0, 0, 0)];

    while stack.len() > 0 {
        let p = stack.pop().unwrap();
        if field[(p.z, p.y, p.x)] != CellType::Inside {
            continue;
        }

        field[(p.z, p.y, p.x)] = CellType::Outside;

        for &[dx, dy, dz] in [
            [-1isize, 0, 0],
            [1, 0, 0],
            [0, -1, 0],
            [0, 1, 0],
            [0, 0, -1],
            [0, 0, 1],
        ]
        .iter()
        {
            let x = p.x.wrapping_add(dx as usize);
            let y = p.y.wrapping_add(dy as usize);
            let z = p.z.wrapping_add(dz as usize);
            if x >= dim.2 || y >= dim.1 || z >= dim.0 || field[(z, y, x)] != CellType::Inside {
                continue;
            }
            stack.push(Point3::new(x, y, z));
        }
    }
}

fn diffuse(field: &mut Array3<Vector3<f32>>, temp: &mut Array3<Vector3<f32>>) {
    let dim = field.dim();
    assert_eq!(temp.dim(), dim);

    // FIXME: remove anisotropy

    for z in 0..dim.0 {
        for y in 0..dim.1 {
            for x in 1..dim.2 - 1 {
                let t = field[(z, y, x)] * 2.0 + field[(z, y, x - 1)] + field[(z, y, x + 1)];
                temp[(z, y, x)] = t * (1.0 / 4.0);
            }
        }
    }
    for z in 0..dim.0 {
        for y in 1..dim.1 - 1 {
            for x in 0..dim.2 {
                let t = temp[(z, y, x)] * 2.0 + temp[(z, y - 1, x)] + temp[(z, y + 1, x)];
                field[(z, y, x)] = t * (1.0 / 4.0);
            }
        }
    }
    for z in 1..dim.0 - 1 {
        for y in 0..dim.1 {
            for x in 0..dim.2 {
                let t = field[(z, y, x)] * 2.0 + field[(z - 1, y, x)] + field[(z + 1, y, x)];
                temp[(z, y, x)] = t * (1.0 / 4.0);
            }
        }
    }

    std::mem::swap(field, temp);
}

fn conservation(
    field: &mut Array3<Vector3<f32>>,
    field_div: &mut Array3<f32>,
    cell_size: Vector3<f32>,
) {
    let dim = field.dim();
    assert_eq!(field_div.dim(), dim);

    for z in 1..dim.0 - 1 {
        for y in 1..dim.1 - 1 {
            for x in 1..dim.2 - 1 {
                let div = (field[(z, y, x + 1)].x - field[(z, y, x - 1)].x) * cell_size.x
                    + (field[(z, y + 1, x)].y - field[(z, y - 1, x)].y) * cell_size.y
                    + (field[(z + 1, y, x)].z - field[(z - 1, y, x)].z) * cell_size.z;
                field_div[(z, y, x)] = div;
            }
        }
    }

    let cell_inv = Vector3::from([0.25; 3]).div_element_wise(cell_size);

    for z in 1..dim.0 - 1 {
        for y in 1..dim.1 - 1 {
            for x in 1..dim.2 - 1 {
                field[(z, y, x)] += Vector3::new(
                    field_div[(z, y, x + 1)] - field_div[(z, y, x - 1)],
                    field_div[(z, y + 1, x)] - field_div[(z, y - 1, x)],
                    field_div[(z + 1, y, x)] - field_div[(z - 1, y, x)],
                )
                .mul_element_wise(cell_inv);
            }
        }
    }
}

#[derive(Debug)]
pub struct Hair {
    vertices: Vec<Point3<f32>>,
    visited: HashSet<u32>,
}

const HAIR_MAX_VERTS: usize = 512;
const CYCLE_DETECTION_DELAY: usize = 5;

impl Hair {
    pub fn new() -> Self {
        Self {
            vertices: Vec::with_capacity(HAIR_MAX_VERTS),
            visited: HashSet::with_capacity(HAIR_MAX_VERTS),
        }
    }

    pub fn update(&mut self, domain: &Domain, start: Point3<f32>) -> Result<(), String> {
        let cell_size = domain.cell_size;

        // Clear the state
        self.vertices.clear();
        self.visited.clear();

        // Offset the starting point so that it's on the inside of the domain
        let mut current = start;
        if domain.get_cell_type_global(current) != CellType::Inside {
            let nearest = domain
                .find_nearest_inner_cell(current)
                .ok_or_else(|| "Could not find the starting point.".to_owned())?;

            let nearest = nearest - Point3::new(0, 0, 0);

            current = domain.domain_min
                + domain
                    .cell_size
                    .mul_element_wise(nearest.cast::<f32>().unwrap());
        }

        self.vertices.push(current);

        let point_to_hash = |p| -> u32 {
            let co = domain.global_to_cell_isize(p);
            (co.x as u32) | ((co.y as u32) << 10) | ((co.z as u32) << 20)
        };

        for _ in 1..HAIR_MAX_VERTS {
            let mut vel = domain.get_velocity_global(current);

            // Scale the velocity vector according to the cell size
            let scale = [
                cell_size.x / vel.x.abs(),
                cell_size.y / vel.y.abs(),
                cell_size.z / vel.z.abs(),
            ]
            .fmin();
            vel *= scale;

            // If `scale` is not a finite, that means we are stuck
            if !scale.is_finite() {
                break;
            }

            // Update the current position
            current += vel;

            // Cycle detection
            if self.visited.contains(&point_to_hash(current)) {
                break;
            }

            // Did we leave the domain?
            if domain.get_cell_type_global(current) == CellType::Outside {
                break;
            }

            // Add the vertex
            self.vertices.push(current);

            if self.vertices.len() > CYCLE_DETECTION_DELAY {
                let p = self.vertices[self.vertices.len() - 1 - CYCLE_DETECTION_DELAY];
                self.visited.insert(point_to_hash(p));
            }
        }

        Ok(())
    }

    pub fn get_sample(&self, percentage: f32) -> Point3<f32> {
        let i_f = percentage * (self.vertices.len() - 1) as f32;
        let i_f = [[i_f, 0.0].fmax(), (self.vertices.len() - 1) as f32].fmin();
        let i = i_f as usize;
        let f = i_f - i as f32;

        let p1 = self.vertices[i];
        let p2 = self.vertices.get(i + 1).cloned().unwrap_or(p1);

        p1 + (p2 - p1) * f
    }
}
