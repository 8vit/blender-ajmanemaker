//! The core library of the AJ Mane Maker addon.
use cgmath::Point3;
use std::{cell::RefCell, ffi::CString, os::raw::c_char};

thread_local! {
    static LAST_ERROR: RefCell<Option<CString>> = RefCell::new(None);
}

/// Set `LAST_ERROR` from `Option<String>`.
fn set_last_error(error: Option<String>) {
    LAST_ERROR.with(|last_error_cell| {
        *last_error_cell.borrow_mut() = error.map(|s| CString::new(s).unwrap());
    });
}

/// Update `LAST_ERROR` according to `result`.
/// Returns `result.unwrap_or_else(fallback)`.
fn catch_error<T>(result: Result<T, String>, fallback: impl FnOnce() -> T) -> T {
    match result {
        Ok(x) => {
            set_last_error(None);
            x
        }
        Err(error) => {
            set_last_error(Some(error));
            fallback()
        }
    }
}

/// Update `LAST_ERROR` according to `result`.
/// Returns `result.unwrap_or(std::ptr::null_mut())`.
fn catch_error_ptr_mut<T>(result: Result<*mut T, String>) -> *mut T {
    catch_error(result, std::ptr::null_mut)
}

/// Update `LAST_ERROR` according to `result`.
/// Returns `result.unwrap_or(T::default())`.
fn catch_error_default<T: Default>(result: Result<T, String>) -> T {
    catch_error(result, T::default)
}

#[no_mangle]
pub extern "C" fn ajmm_last_error() -> *const c_char {
    LAST_ERROR.with(|last_error_cell| {
        if let Some(e) = &*last_error_cell.borrow() {
            e.as_ptr()
        } else {
            std::ptr::null()
        }
    })
}

mod domain;
mod tri;
mod utils;

#[no_mangle]
pub extern "C" fn ajmm_domain_new(
    num_points: usize,
    points: *const f32,
    num_indices: usize,
    indices: *const isize,
    num_iterations: usize,
    domain_resolution: usize,
) -> *mut domain::Domain {
    let points = unsafe { std::slice::from_raw_parts(points, num_points * 4) };

    let points_vec: Vec<_> = points
        .chunks_exact(4)
        .map(|vertex| Point3::new(vertex[0], vertex[1], vertex[2]))
        .collect();

    let points_inflow = points.chunks_exact(4).map(|vertex| vertex[3]).collect();

    let indices = unsafe { std::slice::from_raw_parts(indices, num_indices) };
    let mut faces = Vec::with_capacity(indices.iter().filter(|x| **x < 0).count() + 1);
    {
        let mut face = Vec::new();
        for &i in indices.iter() {
            if i < 0 {
                faces.push(face);
                face = Vec::new();
            } else {
                face.push(i as usize);
            }
        }
    }

    catch_error_ptr_mut(
        domain::Domain::from_mesh(
            points_vec,
            points_inflow,
            faces,
            num_iterations,
            domain_resolution,
        )
        .map(|domain| Box::into_raw(Box::new(domain))),
    )
}

#[no_mangle]
pub extern "C" fn ajmm_domain_free(this: *mut domain::Domain) {
    catch_error_default(unsafe {
        Box::from_raw(this);
        Ok(())
    })
}

#[no_mangle]
pub extern "C" fn ajmm_hair_new() -> *mut domain::Hair {
    catch_error_ptr_mut(Ok(Box::into_raw(Box::new(domain::Hair::new()))))
}

#[no_mangle]
pub extern "C" fn ajmm_hair_free(this: *mut domain::Hair) {
    catch_error_default(unsafe {
        Box::from_raw(this);
        Ok(())
    })
}

#[no_mangle]
pub extern "C" fn ajmm_hair_update(
    this: *mut domain::Hair,
    domain: *const domain::Domain,
    start: Point3<f32>,
) {
    catch_error_default(unsafe {
        let this = &mut *this;
        let domain = &*domain;
        this.update(domain, start)
    })
}

#[no_mangle]
pub extern "C" fn ajmm_hair_get_samples(
    this: *const domain::Hair,
    num_samples: usize,
    out_samples: *mut Point3<f32>,
    length: f32,
) {
    catch_error_default(unsafe {
        let this = &*this;

        let out_samples = std::slice::from_raw_parts_mut(out_samples, num_samples);
        for (i, s) in out_samples.iter_mut().enumerate() {
            *s = this.get_sample(i as f32 * (length / (num_samples - 1) as f32));
        }

        Ok(())
    })
}
