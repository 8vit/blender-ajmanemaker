use alt_fp::FloatOrd;
use cgmath::{Point3, prelude::*};

pub fn point3_min(source: impl IntoIterator<Item = Point3<f32>>) -> Point3<f32> {
    use std::f32::INFINITY;
    source
        .into_iter()
        .fold(Point3::new(INFINITY, INFINITY, INFINITY), |a, b| {
            Point3::new(a.x.fmin(b.x), a.y.fmin(b.y), a.z.fmin(b.z))
        })
}

pub fn point3_max(source: impl IntoIterator<Item = Point3<f32>>) -> Point3<f32> {
    use std::f32::NEG_INFINITY;
    source.into_iter().fold(
        Point3::new(NEG_INFINITY, NEG_INFINITY, NEG_INFINITY),
        |a, b| Point3::new(a.x.fmax(b.x), a.y.fmax(b.y), a.z.fmax(b.z)),
    )
}

/*
def to_barycentric(p: Vector, a: Vector, b: Vector, c: Vector) -> Tuple[float, float, float]:
    """
    https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
    """
    v0, v1, v2 = b - a, c - a, p - a
    d00, d01, d11 = v0.dot(v0), v0.dot(v1), v1.dot(v1)
    d20, d21 = v2.dot(v0), v2.dot(v1)
    denom = d00 * d11 - d01 * d01
    v = (d11 * d20 - d01 * d21) / denom
    w = (d00 * d21 - d01 * d20) / denom
    u = 1 - v - w

    return u, v, w
    */

pub fn point_to_tri_barycentric(p: Point3<f32>, [a, b, c]: [Point3<f32>; 3]) -> [f32; 3] {
    // https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
    let (v0, v1, v2) = (b - a, c - a, p - a);
    let (d00, d01, d11) = (v0.dot(v0), v0.dot(v1), v1.dot(v1));
    let (d20, d21) = (v2.dot(v0), v2.dot(v1));
    let denom = d00 * d11 - d01 * d01;
    let v = (d11 * d20 - d01 * d21) / denom;
    let w = (d00 * d21 - d01 * d20) / denom;
    let u = 1.0 - v - w;
    [u, v, w]
}

/// Provides vertex indices for triangles inside a triangle fan.
#[derive(Debug, Clone, Copy)]
pub struct TriangleFan {
    next: usize,
    count: usize,
}

impl TriangleFan {
    pub fn new(count: usize) -> Self {
        Self { next: 1, count }
    }
}

impl Iterator for TriangleFan {
    type Item = [usize; 3];

    fn next(&mut self) -> Option<Self::Item> {
        if self.next + 1 >= self.count {
            None
        } else {
            let ret = [0, self.next, self.next + 1];
            self.next += 1;
            Some(ret)
        }
    }
}
