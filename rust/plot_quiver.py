import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import math

quivers = np.array([
  # ...
])
X, Y, U, V = np.transpose(quivers)

for i in range(len(X)):
  u, v = U[i], V[i]
  sq = math.sqrt(u * u + v * v) * 100
  if sq > 1:
    u /= sq; v /= sq
    U[i] = u; V[i] = v

fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
ax = fig.add_subplot(111)
# ax.quiver(x, y, z, u, v, w)
ax.quiver(X, Y, U, V)
ax.set_xlim([0,  64])
ax.set_ylim([0, 64])
# ax.set_zlim([0, 16])
plt.show()
