# AJ Mane Maker

A Blender addon for shaping a hair particle system based on a volumetric guide mesh.

## Prerequisite

This addon requires a custom build of Blender 2.8 with the following patches:

- [Work-around T60930](https://gitlab.com/snippets/1833344)
- [Add `particle.edited_set` operator](https://gitlab.com/snippets/1844598)

The core part of this addon is implemented using native code, which requires [the Rust compiler](https://www.rust-lang.org) to build. The minimum required version is Rust 1.35.0.

## Building

    $ cargo build --release

## License

This program is licensed under GNU General Public License v3.0 or later.
