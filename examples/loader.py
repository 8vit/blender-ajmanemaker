# This stub runs a python script relative to the currently open
# blend file, useful when editing scripts externally.

import bpy, os, sys, importlib

modules = [
    'ajmanemaker',
    'ajmanemaker.core',
    'ajmanemaker.interface',
    'ajmanemaker.operator',
]

register_modules = [
    'ajmanemaker',
]

path = os.path.dirname(os.path.dirname(__file__))
if path not in sys.path:
    sys.path.append(path)

modules = [importlib.import_module(name) for name in modules]

for m in modules:
    importlib.reload(m)

for name in register_modules:
    importlib.import_module(name).register()

del sys.path[sys.path.index(path)]
