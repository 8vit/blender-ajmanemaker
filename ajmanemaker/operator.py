import bpy
import bmesh
import random
from mathutils import Vector, Matrix

from .core import AjmmCore
from .interface import AjmmSettings


TEMP_UV_LAYER_NAME = 'AjMMFlow'


class ExecuteAjmm(bpy.types.Operator):
    bl_label = 'Run AJ Mane Maker'
    bl_idname = 'ajmanemaker.execute'
    bl_description = "Soup's on, everypony"
    bl_options = {'UNDO', 'REGISTER'}

    def execute(self, context: bpy.types.Context):
        with AjmmCore() as core:
            # Get the particle system to edit
            edit_obj = context.active_object
            assert edit_obj is not None, "No active object"

            edit_ps = edit_obj.particle_systems.active
            assert edit_ps is not None, "No particle system"

            # Get the ParticleSystemModifier
            ps_mod = [m for m in edit_obj.modifiers if isinstance(m, bpy.types.ParticleSystemModifier)]
            assert len(ps_mod) > 0, "ParticleSystemModifier not found"
            ps_mod = [m for m in ps_mod if m.particle_system == edit_ps]
            assert len(ps_mod) == 1, "Could not find a matching ParticleSystemModifier"
            ps_mod = ps_mod[0]

            # Get the settings
            settings: AjmmSettings = edit_ps.settings.ajmm_settings

            guide_obj: bpy.types.Object = settings.guide_obj
            assert guide_obj is not None, "No guide object"
            assert guide_obj.type == 'MESH', "The guide object is not a mesh"

            inflow_group = guide_obj.vertex_groups[settings.vtx_group_inflow]
            outflow_group = guide_obj.vertex_groups[settings.vtx_group_outflow]
            assert inflow_group, \
                "The guide object doesn't have a vertex group '%s'" % settings.vtx_group_inflow
            assert outflow_group, \
                "The guide object doesn't have a vertex group '%s'" % settings.vtx_group_outflow

            # `BMesh.from_object` disregards vertex weights but not UV layers,
            # so temporarily convert the inflow/outflow info to UV
            uv_layer = guide_obj.data.uv_layers.new(name=TEMP_UV_LAYER_NAME)
            for loop in guide_obj.data.loops:
                inflow = 0
                outflow = 0
                try:
                    inflow = inflow_group.weight(loop.vertex_index)
                except RuntimeError:
                    pass
                try:
                    outflow = outflow_group.weight(loop.vertex_index)
                except RuntimeError:
                    pass

                uv_layer.data[loop.index].uv = Vector((inflow - outflow, 0))

            # Activate the guide object to flush cache. (Otherwise
            # the generated BMesh won't have the generated UV layer)
            guide_obj.select_set(True)
            context.view_layer.objects.active = guide_obj
            assert context.view_layer.objects.active == guide_obj
            bpy.ops.object.editmode_toggle()
            bpy.ops.object.editmode_toggle()
            guide_obj.select_set(False)
            context.view_layer.objects.active = edit_obj

            # Import the guide mesh
            try:
                guide_bmesh: bmesh.BMesh = bmesh.new()
                guide_bmesh.from_object(guide_obj, bpy.context.depsgraph)
                guide_bmesh.transform(guide_obj.matrix_world)

                # Delete the temporary UV layer
                guide_obj.data.uv_layers.remove(guide_obj.data.uv_layers[TEMP_UV_LAYER_NAME])

                points = []
                faces = []

                inflow_layer = guide_bmesh.loops.layers.uv.get(TEMP_UV_LAYER_NAME)
                assert inflow_layer is not None

                for v in guide_bmesh.verts:
                    points.append(v.co[0])
                    points.append(v.co[1])
                    points.append(v.co[2])
                    points.append(v.link_loops[0][inflow_layer].uv[0])

                for f in guide_bmesh.faces:
                    for v in f.verts:
                        faces.append(v.index)
                    faces.append(-1)

            finally:
                guide_bmesh.free()

            # RNG
            random_length_rng = random.Random(1)

            domain_opts = {
                'points': points,
                'indices': faces,
                'num_iterations': settings.num_iterations,
                'domain_resolution': int(settings.domain_resolution),
            }

            with core.domain_new(**domain_opts) as core_domain, core.hair_new() as core_hair:
                # Clear the existing edit data
                bpy.ops.particle.edited_clear()

                bpy.ops.particle.particle_edit_toggle()
                bpy.context.scene.tool_settings.particle_edit.use_emitter_deflect = False
                bpy.context.scene.tool_settings.particle_edit.use_preserve_root = False
                bpy.context.scene.tool_settings.particle_edit.use_preserve_length = False

                bpy.ops.particle.edited_set()
                bpy.ops.particle.disconnect_hair(all=True)
                bpy.ops.particle.connect_hair(all=True)

                mat = edit_obj.matrix_world

                for part in edit_ps.particles:
                    keys = part.hair_keys

                    hmat = mat @ particle_get_hair_matrix(part, edit_obj, ps_mod)
                    origin = hmat @ Vector((0, 0, 0))
                    hmat_inv = hmat.inverted()

                    # Shape the hair strand
                    core_hair.update(core_domain, origin)
                    len_scale = 1 - settings.random_length * random_length_rng.random()
                    samples = core_hair.get_samples(len(part.hair_keys), len_scale)

                    for i, key in enumerate(part.hair_keys):
                        co = samples[i]
                        co_l = hmat_inv @ co
                        key.co_local = co_l

                bpy.ops.particle.particle_edit_toggle()

            return {'FINISHED'}


def particle_get_hair_matrix(part: bpy.types.Particle, obj: bpy.types.Object, p_mod: bpy.types.ParticleSystemModifier):
    """
    Calculate the hair matrix (local -> object) destructively
    for a given Object 'obj', Particle 'p', and ParticleSystemModifier 'p_mod'.
    """
    d = bpy.context.depsgraph
    key = part.hair_keys[0]
    key.co_local = Vector((0, 0, 0))
    p0 = key.co_object(obj, p_mod, part, d)
    key.co_local = Vector((1, 0, 0))
    p1 = key.co_object(obj, p_mod, part, d) - p0
    key.co_local = Vector((0, 1, 0))
    p2 = key.co_object(obj, p_mod, part, d) - p0
    key.co_local = Vector((0, 0, 1))
    p3 = key.co_object(obj, p_mod, part, d) - p0

    return Matrix((
        (p1[0], p2[0], p3[0], p0[0]),
        (p1[1], p2[1], p3[1], p0[1]),
        (p1[2], p2[2], p3[2], p0[2]),
        (0, 0, 0, 1),
    ))


def register():
    bpy.utils.register_class(ExecuteAjmm)


def unregister():
    bpy.utils.unregister_class(ExecuteAjmm)
