from . import interface, operator


def register():
    operator.register()
    interface.register()


def unregister():
    interface.unregister()
    operator.unregister()
