import ctypes
from ctypes import CDLL, pointer
from os import path
import sys
from typing import List
from mathutils import Vector


class AjmmCore(object):
    def __init__(self):
        # Locate the core library
        lib_path = path.join(path.dirname(__file__), '..', 'target', 'release', 'libajmm_core')

        if sys.platform.startswith('darwin'):
            lib_path += '.dylib'
        elif sys.platform.startswith('win32'):
            lib_path += '.dll'
        elif sys.platform.startswith('cygwin'):
            lib_path += '.dll'
        else:
            lib_path += '.so'

        self.cdll = CDLL(lib_path)

        self.cdll.ajmm_last_error.restype = ctypes.c_char_p
        self.cdll.ajmm_domain_new.restype = ctypes.c_void_p
        self.cdll.ajmm_hair_new.restype = ctypes.c_void_p

    def _check_error(self):
        e = self.cdll.ajmm_last_error()
        if e:
            raise RuntimeError(str(e))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # TODO: unload the library?
        pass

    def domain_new(self,
                   points: List[float],
                   indices: List[int],
                   num_iterations: int,
                   domain_resolution: int) -> 'Domain':
        assert len(points) % 4 == 0
        c_points = (ctypes.c_float * len(points))(*points)
        c_indices = (ctypes.c_ssize_t * len(indices))(*indices)

        domain = self.cdll.ajmm_domain_new(ctypes.c_size_t(len(c_points) // 4),
                                           pointer(c_points),
                                           ctypes.c_size_t(len(c_indices)),
                                           pointer(c_indices),
                                           ctypes.c_size_t(num_iterations),
                                           ctypes.c_size_t(domain_resolution))
        self._check_error()

        return Domain(self, domain)

    def domain_free(self, this: 'Domain'):
        assert isinstance(this, Domain)
        self.cdll.ajmm_domain_free(this.handle)
        self._check_error()
        this.handle = None

    def hair_new(self) -> 'Hair':
        hair = self.cdll.ajmm_hair_new()
        self._check_error()
        return Hair(self, hair)

    def hair_free(self, this: 'Hair'):
        assert isinstance(this, Hair)
        self.cdll.ajmm_hair_free(this.handle)
        self._check_error()
        this.handle = None


class Vector3(ctypes.Structure):
    _fields_ = [
        ('x', ctypes.c_float),
        ('y', ctypes.c_float),
        ('z', ctypes.c_float),
    ]


class Domain(object):
    def __init__(self, core: AjmmCore, handle: ctypes.c_void_p):
        self.__core = core
        self.handle = ctypes.c_size_t(handle)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__core.domain_free(self)


class Hair(object):
    def __init__(self, core: AjmmCore, handle: ctypes.c_void_p):
        self.__core = core
        self.handle = ctypes.c_size_t(handle)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__core.hair_free(self)

    def update(self, domain: Domain, start: Vector):
        assert isinstance(domain, Domain)

        self.__core.cdll.ajmm_hair_update(self.handle, domain.handle, Vector3(start[0], start[1], start[2]))
        self.__core._check_error()

    def get_samples(self, count: int, length: float) -> List[Vector]:
        c_samples = (Vector3 * count)()
        self.__core.cdll.ajmm_hair_get_samples(self.handle,
                                               ctypes.c_size_t(count),
                                               pointer(c_samples),
                                               ctypes.c_float(length))
        self.__core._check_error()

        return [Vector((c_samples[i].x, c_samples[i].y, c_samples[i].z)) for i in range(count)]
