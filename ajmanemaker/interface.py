import bpy


class PARTICLE_PT_Ajmm(bpy.types.Panel):
    bl_label = "AJ Mane Maker"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'particle'
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context: bpy.types.Context):
        psys = context.particle_system
        if psys is None:
            return False
        if psys.settings is None:
            return False
        return psys.settings.type == 'HAIR'

    def draw(self, context: bpy.types.Context):
        layout = self.layout
        psys = context.particle_system
        pset = psys.settings

        col = layout.column()
        row = col.row()
        row.prop(pset.ajmm_settings, 'guide_obj')

        obj = pset.ajmm_settings.guide_obj
        if obj:
            row = col.row()
            row.prop_search(pset.ajmm_settings, 'vtx_group_inflow', obj, 'vertex_groups')

            row = col.row()
            row.prop_search(pset.ajmm_settings, 'vtx_group_outflow', obj, 'vertex_groups')

        row = col.row()
        row.prop(pset.ajmm_settings, 'random_length')

        row = col.row()
        row.prop(pset.ajmm_settings, 'num_iterations')
        row.prop(pset.ajmm_settings, 'domain_resolution')

        row = col.row()
        row.operator('ajmanemaker.execute')


_domain_resolution_items = [(str(1 << i), str(1 << i), '') for i in range(4, 9)]


class AjmmSettings(bpy.types.PropertyGroup):
    guide_obj: bpy.props.PointerProperty(name="Guide",
                                         type=bpy.types.Object,
                                         description="Hair flows according to the guide object.",
                                         poll=lambda _,obj: obj.type == 'MESH')

    vtx_group_inflow: bpy.props.StringProperty(name="Inflow",
                                               description="The vertex group specifying the amount of hair entering "
                                                           "through the corresponding faces.")

    vtx_group_outflow: bpy.props.StringProperty(name="Outflow",
                                                description="The vertex group specifying the amount of hair leaving "
                                                            "through the corresponding faces.")

    num_iterations: bpy.props.IntProperty(name="Iterations",
                                          description="Controls the solver iteration count. Affects the output quality.",
                                          min=1,
                                          max=10000,
                                          default=500)

    domain_resolution: bpy.props.EnumProperty(name="Resolution",
                                              description="Controls the solver iteration count. "
                                                          "Affects the output quality and the operation time.",
                                              items=_domain_resolution_items,
                                              default='64')

    random_length: bpy.props.FloatProperty(name="Randomize Length",
                                           description="Randomizes the hair length",
                                           min=0,
                                           max=1,
                                           subtype='FACTOR')

def register():
    bpy.utils.register_class(AjmmSettings)
    bpy.types.ParticleSettings.ajmm_settings = \
        bpy.props.PointerProperty(type=AjmmSettings,
                                  name="AJ Mane Maker Settings")
    bpy.utils.register_class(PARTICLE_PT_Ajmm)


def unregister():
    bpy.utils.unregister_class(PARTICLE_PT_Ajmm)
    del bpy.types.ParticleSettings.ajmm_settings
    bpy.utils.unregister_class(AjmmSettings)


